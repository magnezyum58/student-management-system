<!DOCTYPE html>
<html>  
	<head>	
		<meta charset ="utf-8">
		<title>Öğrenci Bilgi Sistemi</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        	<link rel="stylesheet" href="http://localhost/student-management-system/Css/Announcement_style.css">
    </head>
	<body>
    
        <div class ="icon">
			<img src ="http://localhost/student-management-system/images/icon.gif" width ="50" height="50">
	    </div>
        <div class="container" style="margin-top: 20px">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Duyuru id</td>
                                <td>Duyuru</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                require_once "config.php";
                                $sql = "SELECT * FROM duyuru";
                                if($result = mysqli_query($link, $sql)){
                                    if(mysqli_num_rows($result) > 0){
                                        while($row = mysqli_fetch_array($result)){
                              
                                            echo '
                                                <tr>
                                                    <td>'.$row['duyuru_id'].'</td>
                                                    <td>'.$row['duyuru'].'</td>
                                                </tr>
                                            ';
                                        }
                                    }
                                }
                            ?>
                        </tbody>
                        
		</div>

        <button class="edit-button btn">Düzenle</button>

        
        <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
        </script>
        <script type="text/javascript" src="http://localhost/student-management-system/Js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://localhost/student-management-system/Js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".table").DataTable();
            });
        </script>
		<footer class="footer-class">
			<center>Ögrenci Bilgi Sistemi</center>
		<footer>
        

	</body>
</html>
